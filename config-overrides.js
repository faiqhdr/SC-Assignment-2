const { override } = require('customize-cra');

const overrideJestConfig = config => {
    config.testEnvironment = "jsdom";
    config.setupFilesAfterEnv = ["<rootDir>/jest.setup.js"];
    return config;
};

module.exports = {
    webpack: override(),
    jest: overrideJestConfig
};
