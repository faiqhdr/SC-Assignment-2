import { useEffect, useState } from 'react';
import { collection, query, where, getDocs } from "firebase/firestore";
import { firestore } from '../firebase';
import { useAuth } from '../authContext';

const useFetchEntries = () => {
    const [entries, setEntries] = useState([]);
    const { currentUser } = useAuth();

    useEffect(() => {
        const fetchEntries = async () => {
        if (!currentUser) {
            setEntries([]);
            return;
        }
        try {
            const q = query(collection(firestore, 'cashflows'), where('userId', '==', currentUser.uid));
            const querySnapshot = await getDocs(q);
            const fetchedEntries = querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
            setEntries(fetchedEntries);
        } catch (error) {
            console.error(error);
        }
        };
        fetchEntries();
    }, [currentUser]);

    return entries;
};

export default useFetchEntries;
