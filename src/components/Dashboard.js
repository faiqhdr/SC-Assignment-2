import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { auth } from '../firebase';
import CashFlowForm from './CashFlowForm';
import CashFlowList from './CashFlowList';
import useFetchEntries from '../hooks/useFetchEntries';
import './Dashboard.css';

function Dashboard() {
    const navigate = useNavigate();
    const entries = useFetchEntries();
    const [entryToEdit, setEntryToEdit] = useState(null);
    const [updatedEntries, setUpdatedEntries] = useState([]);

    const handleLogout = async () => {
        try {
            await auth.signOut();
            navigate('/login');
        } catch (error) {
            console.error('Error logging out:', error);
        }
    };

    useEffect(() => {
        setUpdatedEntries(entries);
    }, [entries]);

    return (
        <div className="dashboard-container">
            <h2>Cash Flow App Dashboard</h2>
            <button onClick={handleLogout} className="logout-button">Logout</button>
            <CashFlowForm setEntries={setUpdatedEntries} entryToEdit={entryToEdit} setEntryToEdit={setEntryToEdit} />
            <CashFlowList entries={updatedEntries} setEntries={setUpdatedEntries} setEntryToEdit={setEntryToEdit} />
        </div>
    );
}

export default Dashboard;
