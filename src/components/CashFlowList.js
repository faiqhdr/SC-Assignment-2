import React from 'react';
import { doc, deleteDoc, query, where, getDocs, collection } from "firebase/firestore";
import { firestore } from '../firebase';
import { useAuth } from '../authContext';
import './CashFlowList.css';

function CashFlowList({ entries, setEntries, setEntryToEdit }) {
    const { currentUser } = useAuth();

    const deleteEntry = async (id) => {
        try {
            await deleteDoc(doc(firestore, 'cashflows', id));
            alert('Entry deleted');
            const q = query(collection(firestore, 'cashflows'), where('userId', '==', currentUser.uid));
            const querySnapshot = await getDocs(q);
            const updatedEntries = querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
            setEntries(updatedEntries);
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <div className="cashflow-list-container">
            <h3>Cash Flow Entries</h3>
            <ul>
                {entries.map((entry) => (
                    <li key={entry.id} className="entry-item">
                        <span>{entry.description}: {entry.amount} ({entry.type})</span>
                        <div className="entry-buttons">
                            <button onClick={() => setEntryToEdit(entry)} className="edit-button">Edit</button>
                            <button onClick={() => deleteEntry(entry.id)} className="delete-button">Delete</button>
                        </div>
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default CashFlowList;
