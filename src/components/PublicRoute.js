import React from 'react';
import { Navigate } from 'react-router-dom';
import { useAuth } from '../authContext';

const PublicRoute = ({ element: Component, ...rest }) => {
    const { currentUser } = useAuth();

    return !currentUser ? <Component {...rest} /> : <Navigate to="/dashboard" />;
    };

export default PublicRoute;
